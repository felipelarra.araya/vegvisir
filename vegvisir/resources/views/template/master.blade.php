<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Gruas VegVisir</title>
  <meta name="description" content="Servicio de Grúas y rescate de vehículos en La Serena, Coquimbo y alrededores. trabajamos las 24 horas del día y los 365 días del año." />
  <meta name="keywords" content="gruas en la serena, gruas en coquimbo, gruas coquimbo, gruas cuarta region, servicio de gruas, coquimbo, chile, servicio de gruas en coquimbo, servicio de gruas en la serena, servicio de gruas en la serena" />
  <!-- Favicons -->
  <link href="{{asset('/img/logo/vegvisir-logo.png')}}" rel="icon">
  <link href="{{url('img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{url('vendor/aos/aos.css')}}" rel="stylesheet">
  <link href="{{url('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{url('vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
  <link href="{{url('vendor/boxicons/css/boxicons.min.css')}}"  rel="stylesheet">
  <link href="{{url('vendor/glightbox/css/glightbox.min.css')}}"  rel="stylesheet">
  <link href="{{url('vendor/swiper/swiper-bundle.min.css')}}"  rel="stylesheet">
  <link href="{{url('vendor/remixicon/remixicon.css')}}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{url('css/style.css')}}" rel="stylesheet">

@yield('css')
</head>

<body>
    @include('template.menu')
    <div id="preloader"></div>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
    <a href="https://api.whatsapp.com/send?phone=56942741221" class="btn-wsp "><i class="bx bxl-whatsapp"></i></a>
  

  @section('content')
  @show

  @include('template.footer')

  <!-- Vendor JS Files -->
  <script src="{{url('vendor/purecounter/purecounter_vanilla.js')}}"></script>
  <script src="{{url('vendor/aos/aos.js')}}"></script>
  <script src="{{url('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{url('vendor/glightbox/js/glightbox.min.js')}}"></script>
  <script src="{{url('vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{url('vendor/swiper/swiper-bundle.min.js')}}"></script>




  <!-- Template Main JS File -->
  <script src="{{url('js/main.js')}}"></script>
  @yield('js')
</body>

</html>
