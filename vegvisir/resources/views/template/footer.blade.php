  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="social-links mt-3 text-center">

          <a href="https://www.facebook.com/gruaserenaycoquimbo" class="facebook"><i class="bx bxl-facebook"></i></a>
          <a href="https://www.instagram.com/gruas_vegvisir/" class="instagram"><i class="bx bxl-instagram"></i></a>
          
        </div>
        

         

      

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Gruas Vegvisir</span></strong>. All Rights Reserved
      </div>
    </div>
  </footer><!-- End Footer -->