<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mensaje de contacto</title>
</head>
<body>
    
    <p>Recibiste un mensaje de {{ $msg['name'] }}</p>
    <p>Asunto: {{ $msg['subject'] }}</p>
    <p>Mensaje: {{ $msg['message'] }}</p>
    <p>Email: {{ $msg['email'] }}</p>
</body>
</html>