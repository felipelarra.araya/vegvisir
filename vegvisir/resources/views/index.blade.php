@extends('template.master')
@section('content')


<!-- ======= Hero Section ======= -->
<section id="hero">
    <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">

      <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

      <div class="carousel-inner" role="listbox">

        <!-- Slide 1 -->
        <div class="carousel-item active" style="background-image: url(img/slide/gruas-vegvisir-1.jpeg)">
          <div class="carousel-container">
            <div class="container">
              <h2 class="animate__animated animate__fadeInDown">Gruas <span>Vegvisir</span></h2>
              <p class="animate__animated animate__fadeInUp">Servicio de Grúas y rescate de vehículos Vegvisir, en La Serena,Coquimbo y alrededores.</p>
              <a href="tel:+56942741221" class="btn-get-started animate__animated animate__fadeInUp scrollto"><i class="bx bx-phone"></i> Llámanos ahora</a>
            </div>
          </div>
        </div>

        <!-- Slide 2 -->
        <div class="carousel-item" style="background-image: url(img/slide/gruas-vegvisir-2.jpeg)">
          <div class="carousel-container">
            <div class="container">
              <h2 class="animate__animated animate__fadeInDown">Rescate 4x4</h2>
              <p class="animate__animated animate__fadeInUp">Nuestra empresa cuenta con grúa de levante 4x4. Ideal para vehículos sin ruedas o enterrados</p>
              <a href="tel:+56942741221" class="btn-get-started animate__animated animate__fadeInUp scrollto"><i class="bx bx-phone"></i> Llámanos ahora</a>
            </div>
          </div>
        </div>

        <!-- Slide 3 -->
        <div class="carousel-item" style="background-image: url(img/slide/gruas-vegvisir-3.jpeg)">
        </div>

      </div>

      <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
        <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
      </a>

      <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
        <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
      </a>

    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Sobre nosotros</h2>
          <p>Sobre nosotros</p>
        </div>

        <div class="row content">
          <div class="col-lg-6">
            <p>
              Grúas Vegvisir es una empresa regional, dedicada al rubro de la asistencia en ruta, remolque y rescate de vehículos con desperfectos mecánicos o siniestrados.
            <br>
            <br>
            Nuestra empresa se encuentra compuesta por profesionales del rubro automotriz, quienes trabajan las 24 horas del día y durante los 365 días del año, a fin de poder asistir a sus clientes, cada vez que lo requieran.
            <br>
            <br>
            Nuestro objetivo es prestar un servicio de calidad y oportuno, en los traslados de sus vehículos, tanto en Coquimbo, La Serena, alrededores, y viajes interregionales.
            Confié en nuestra empresa, la cual se encuentra inscrita en el Servicio de Impuestos Internos, pudiendo emitir boleta o factura, dependiendo de los requerimientos de sus clientes.
            Cotice con nosotros, no se arrepentirá.
            </p>
           
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <img class="img-fluid" src={{asset('/img/slide/gruas-vegvisir-1.jpeg')}} alt="gruas-vegvisir">
          </div>
        </div>

      </div>
    </section><!-- End About Section -->


    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Servicios</h2>
          <p>Servicios</p>
        </div>

        <div class="row">
          <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class="bx bxs-time"></i></div>
              <h4><a href="">Disponible Las 24 Hrs</a></h4>
              <p>Rescate y transporte de vehículos las 24 hrs y los 365 días del año.</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class="bx bxs-truck"></i></div>
              <h4><a href="">Transporte De Carga Y Fletes</a></h4>
              <p>Transporte de carga y fletes</p>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-tachometer"></i></div>
              <h4><a href="">Asistencia En Ruta.</a></h4>
              <p>Asistencia en ruta, combustibles, asistencia eléctrica por fallas de batería, escanner, etc.</p>
            </div>
          </div>

          <div class="col-lg-12 col-md-12  align-items-stretch mt-4" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class="bx bxs-map"></i></div>
              <h4><a href="">Asistencia Nacional</a></h4>
              <p>Asistencia dentro y fuera de la región.</p>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Services Section -->

    

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
      <div class="container" data-aos="zoom-in">

        <div class="text-center">
          <h3>Atención Las 24 Hrs</h3>
          <p> Cada vez que nos necesiten ahí estaremos.
            Recuerde que trabajamos las 24 horas del día y los 365 días del año, en Coquimbo, La Serena y alrededores.
            Además realizamos viajes hacia otras regiones.</p>
        </div>

      </div>
    </section><!-- End Cta Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Galeria</h2>
          <p>Galeria</p>
        </div>
        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

          <div class="col-lg-4 col-md-6 portfolio-item">
            <a href="{{asset('/img/galeria/gruas-vegvisir-1.jpeg')}}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link">
            <img src="{{asset('/img/galeria/gruas-vegvisir-1.jpeg')}}" class="img-gallery" alt="gruas-vegvisir-coquimbo">
            </a>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item">
            <a href="{{asset('/img/galeria/gruas-vegvisir-2.jpeg')}}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link">
            <img src="{{asset('/img/galeria/gruas-vegvisir-2.jpeg')}}" class="img-gallery" alt="gruas-la-serena-coquimbo">
            </a>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item">
            <a href="{{asset('/img/galeria/gruas-vegvisir-3.jpeg')}}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link">
            <img src="{{asset('/img/galeria/gruas-vegvisir-3.jpeg')}}" class="img-gallery" alt="gruas-vegvisir">
            </a>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item">
            <a href="{{asset('/img/galeria/gruas-vegvisir-4.jpeg')}}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link">
            <img src="{{asset('/img/galeria/gruas-vegvisir-4.jpeg')}}" class="img-gallery" alt="gruas-vegvisir">
            </a>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item">
            <a href="{{asset('/img/galeria/gruas-vegvisir-5.jpeg')}}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link">
            <img src="{{asset('/img/galeria/gruas-vegvisir-5.jpeg')}}" class="img-gallery" alt="gruas-vegvisir">
            </a>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item">
            <a href="{{asset('/img/galeria/gruas-vegvisir-6.jpeg')}}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link">
            <img src="{{asset('/img/galeria/gruas-vegvisir-6.jpeg')}}" class="img-gallery" alt="gruas-vegvisir">
            </a>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item">
            <a href="{{asset('/img/galeria/gruas-vegvisir-7.jpeg')}}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link">
            <img src="{{asset('/img/galeria/gruas-vegvisir-7.jpeg')}}" class="img-gallery" alt="gruas-vegvisir">
            </a>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item">
            <a href="{{asset('/img/galeria/gruas-vegvisir-8.jpeg')}}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link">
            <img src="{{asset('/img/galeria/gruas-vegvisir-8.jpeg')}}" class="img-gallery" alt="gruas-vegvisir">
            </a>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item">
            <a href="{{asset('/img/galeria/gruas-vegvisir-9.jpeg')}}" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link">
            <img src="{{asset('/img/galeria/gruas-vegvisir-9.jpeg')}}" class="img-gallery" alt="gruas-vegvisir">
            </a>
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Section -->

  

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Contacto</h2>
          <p>Contacto</p>
        </div>

        <div class="row">
          @if($message = Session::get('success'))
          <div class="col-12 alert alert-success alert-dismissable fade show" id="mensaje" role="alert"> 
          <span>{{ $message }}</span>  
        </div>
        @endif
          @if ($errors->any())
          <div class="alert alert-danger" role="alert">
            <h4 class="is-size-4">Por favor, valida los siguientes errores:</h4>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
          <div class="col-lg-6">

            <div class="row">
              <div class="col-md-12">
                <div class="info-box">
                  <i class="bx bx-map"></i>
                  <h3>Nuestra Ubicación</h3>
                  <p>Las Orquídeas 881, Sindempart, Coquimbo, Chile</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="info-box mt-4">
                  <i class="bx bx-envelope"></i>
                  <h3>Email</h3>
                  <p>transportesvegvisir@gmail.com</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="info-box mt-4">
                  <i class="bx bx-phone-call"></i>
                  <h3>Teléfono</h3>
                  <p>+569 42741221</p>
                </div>
              </div>
            </div>

          </div>
          <div class="col-lg-6">
            <form action="{{ route('store.contact') }}" method="post" role="form" class="php-email-form">
              @csrf
              <div class="row">
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Tu Nombre" required>
                </div>
                <div class="col-md-6 form-group mt-3 mt-md-0">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Tu correo" required>
                </div>
              </div>
              <div class="form-group mt-3">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Asunto" required>
              </div>
              <div class="form-group mt-3">
                <textarea class="form-control" name="message" rows="5" placeholder="Mensaje" required></textarea>
              </div>
              <div class="text-center mt-3"><button type="submit">Enviar mensaje</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  @section('js')
  <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
  <script>
    $(document).ready(function() {
            
          
            setTimeout(function() {
                $("#mensaje").fadeOut(1500);
            },3000);
          
        });
        
    </script>
  @endsection
  @endsection
