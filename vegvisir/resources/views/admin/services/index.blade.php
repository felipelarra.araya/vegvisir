@extends('admin.template.master')
@section('title', 'Servicios')

@section('contenido')

<div class="container body">
    <div class="main_container">
      @include('admin.template.menu')
      @include('admin.template.top_nav')
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="page-title">
          <div class="title_left">
            <h3>Servicios</h3>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
              <div class="x_title">
                <a class="btn btn-primary" href="" role="button">Agregar servicio</a>
                
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="card-box table-responsive">
                      <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                          <tr>
                            <th>Imagen</th>
                            <th>Nombre</th>
                            <th>Acción</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            {{-- @foreach($services as $item)
                            <td><img src="{{asset('/images/services/thumbs/'.$item->image)}}" width="100"></td>
                            <td> {{$item->name}} </td>

                            <td> 
                              <div class="row">
                                <a class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Actualizar servicio" href="{{ route ('services.edit',$item->id)}}"> <i class="fa fa-pencil-square-o"></i></a>
                                <form action="{{ route ('services.destroy',$item->id)}}" class="form-eliminar" method="POST">
                                @csrf
                                @method('DELETE')  
                                  <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar servicio"><i class="fa fa-trash"></i></button>
                              </form>
                              </div>
                           </td>
                          </tr>
                          @endforeach --}}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /page content -->
      @include('admin.template.footer')
    </div>
  </div>
@endsection

@section('scripts')

<script>

$(document).ready(function() {
      toastr.options.timeOut = 1500;
      @if (Session::has('error'))
          toastr.error('{{ Session::get('error') }}');
      @elseif(Session::has('success'))
          toastr.success('{{ Session::get('success') }}');
      @elseif(Session::has('eliminado'))
      toastr.success('{{ Session::get('eliminado') }}');
      @endif
    });

$('.form-eliminar').submit(function(e){
      e.preventDefault();
      Swal.fire({
      title: "¿Estas seguro?",
      text: "Una vez eliminado, no podrá recuperar esta información!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
      this.submit();
      }
    })
});



  $('#datatable').DataTable({
      "lengthMenu": [[5, 10, 50, -1 ], [5, 10, 50, "Todos"]],
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros ",
            "zeroRecords": "Nada encontrado",
            "info": "Mostrando la página _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar",
            'paginate':{
              'next':'Siguiente',
              'previous':'Anterior'
            }
        }
    });
</script>
@endsection