@extends('admin.template.master')
@section('title', 'Servicios')

@section('contenido')
<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      @include('admin.template.menu')
      @include('admin.template.top_nav')
      <!-- page content -->
      <div class="right_col" role="main">
        <div class="page-title">
          <div class="title_left">
            <h3>Editar Servicio</h3>
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12 col-sm-12 ">
            <div class="x_panel">
              <div class="x_content">
                <div class="row">
                  <div class="col-sm-12">
                    <br><br>
                    <form class="form-horizontal form-label-left" enctype="multipart/form-data" action="{{ route('services.update',$service->id) }}" method="POST">
                      @csrf
                      @method('PUT')
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="imagen">Imagen o Icono del servicio <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                          <input type="file" name="image" id="imagen" accept="image/jpg,image/jpeg,image/bmp,image/png" >
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="titulo">Nombre del Servicio <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                          <input type="text" id="titulo" name="name" value="{{ $service->name }}" class="form-control" required>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align" for="descripcion">Breve Descripción del Servicio <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6">
                          <textarea name="description" id="descripcion" class="form-control" rows="3" required style="resize: none" maxlength="250" >{{ $service->description }}</textarea>
                        </div>
                      </div>
                      @if ($errors->any())
                        <div class="alert alert-danger" role="alert">
                          <h4 class="is-size-4">Por favor, valida los siguientes errores:</h4>
                          <ul>
                            @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                            @endforeach
                          </ul>
                        </div>
                      @endif
                      <div class="ln_solid"></div>
                      <div class="item form-group">
                        <div class="col-md-6 col-sm-6 offset-md-3">
                          <button type="submit" class="btn btn-success">Actualizar</button>
                        </div>
                      </div>  
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>        
      </div>
      <!-- /page content -->
      @include('admin.template.footer')
    </div>
  </div>
</body>
@endsection