<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Vegvisir @yield('titulo')</title>

  <!-- Bootstrap -->
  <link href="{{url('admin/assets/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="{{url('admin/assets/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <!-- NProgress -->
  <link href="{{url('admin/assets/vendors/nprogress/nprogress.css')}}" rel="stylesheet">
  <!-- Animate.css -->
  <link href="{{url('admin/assets/vendors/animate.css/animate.min.css')}}" rel="stylesheet">
  <!-- Datatables -->
  <link href="{{url('admin/assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{url('admin/assets/build/css/custom.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  @yield('css')
  
</head>
<body class="nav-md">
  @yield('contenido')
   


  <!-- jQuery -->
<script src="{{url('admin/assets/vendors/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{url('admin/assets/vendors/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<!-- FastClick -->
<script src="{{url('admin/assets/vendors/fastclick/lib/fastclick.js')}}"></script>
<!-- NProgress -->
<script src="{{url('admin/assets/vendors/nprogress/nprogress.js')}}"></script>
<!-- Chart.js -->
<script src="{{url('admin/assets/vendors/Chart.js/dist/Chart.min.js')}}"></script>
<!-- gauge.js -->
<script src="{{url('admin/assets/vendors/gauge.js/dist/gauge.min.js')}}"></script>
<!-- bootstrap-progressbar -->
<script src="{{url('admin/assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
<!-- iCheck -->
<script src="{{url('admin/assets/vendors/iCheck/icheck.min.js')}}"></script>
<!-- Skycons -->
<script src="{{url('admin/assets/vendors/skycons/skycons.js')}}"></script>
<!-- Flot -->
<script src="{{url('admin/assets/vendors/Flot/jquery.flot.js')}}"></script>
<script src="{{url('admin/assets/vendors/Flot/jquery.flot.pie.js')}}"></script>
<script src="{{url('admin/assets/vendors/Flot/jquery.flot.time.js')}}"></script>
<script src="{{url('admin/assets/vendors/Flot/jquery.flot.stack.js')}}"></script>
<script src="{{url('admin/assets/vendors/Flot/jquery.flot.resize.js')}}"></script>
<!-- Flot plugins -->
<script src="{{url('admin/assets/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
<script src="{{url('admin/assets/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
<script src="{{url('admin/assets/vendors/flot.curvedlines/curvedLines.js')}}"></script>
<!-- DateJS -->
<script src="{{url('admin/assets/vendors/DateJS/build/date.js')}}"></script>
<!-- JQVMap -->
<script src="{{url('admin/assets/vendors/jqvmap/dist/jquery.vmap.js')}}"></script>
<script src="{{url('admin/assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
<script src="{{url('admin/assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>
<!-- bootstrap-daterangepicker -->
<script src="{{url('admin/assets/vendors/moment/min/moment.min.js')}}"></script>
<script src="{{url('admin/assets/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- Datatables -->
<script src="{{url( 'admin/assets/vendors/datatables.net/js/jquery.dataTables.min.js' )}}"></script>
<script src="{{url( 'admin/assets//vendors/datatables.net-bs/js/dataTables.bootstrap.min.js' )}}"></script>
<!-- Custom Theme Scripts -->
<script src="{{url('admin/assets/build/js/custom.min.js')}}"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@yield('scripts') 
</body>

</html>
