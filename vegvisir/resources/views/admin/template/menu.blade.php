<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="{{url('/dashboard')}}" class="site_title"><span>VegVisir</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_info">
        <span>Bienvenido: </span>
        <h2>{{ session('username') }}</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">
          
          <li><a href="{{ url('admin/services') }}"><i class="fa fa-wrench"></i>Servicios</a></li>
          <li><a href="{{ url('admin/products') }}"><i class="fa fa-laptop"></i>Productos</a></li>
          <li><a href="{{ url('/admin/about') }}"><i class="fa fa-info-circle"></i>Sobre Nosotros</a></li>
          <li><a href="{{ url('/admin/team') }}"><i class="fa fa-users"></i>Equipo</a></li>
          <li><a href="{{ url('/admin/portfolio') }}"><i class="fa fa-briefcase"></i>Portafolio</a></li>
          <li><a href="{{ url('/admin/categories') }}"><i class="fa fa-desktop"></i>Categorias</a></li>
     
        </ul>
      </div>

    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="Configuración"  href="{{ url('/administracion/configuracion') }}">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
      
      <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{ url('/logout') }}">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>
