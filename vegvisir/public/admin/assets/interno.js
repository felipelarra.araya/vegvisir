function mostrarPassword(){
  var cambio = document.getElementById("password");
  if(cambio.type == "password"){
    cambio.type = "text";
    $('#password-eye').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
  }else{
    cambio.type = "password";
    $('#password-eye').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
  }
}

function mostrarconfirmPassword() {
  var cambio = document.getElementById("confirm-password");
  if(cambio.type == "password"){
    cambio.type = "text";
    $('#confirm-password-eye').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
  }else{
    cambio.type = "password";
    $('#confirm-password-eye').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
  }
}