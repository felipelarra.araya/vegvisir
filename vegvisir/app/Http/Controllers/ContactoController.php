<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\MessageReceived;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactFormRequest;

class ContactoController extends Controller
{
    public function send(ContactFormRequest $request)
    {
        
        try {
                 
            $msg = $request->all();
            
            Mail::to(env('MAIL_FROM_ADDRESS'))->send(new MessageReceived($msg));
            return redirect()->to(route('home') . '#contact')->with('success','Mensaje enviado con éxito');
           
               
        } catch (\Exception $e) {
            \Log::error(__METHOD__ . ' ::: Error storing contact [ ' . $e->getMessage() . ' ]');
            return redirect()->to(route('home') . '#contact')->with('error','Error intente más tarde');
        }

    }
}
