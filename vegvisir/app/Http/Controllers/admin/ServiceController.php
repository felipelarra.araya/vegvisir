<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ServiceRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use App\Models\Service;
use Image;


class ServiceController extends Controller
{


    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$services = Service::all();
      return view('admin.services.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequest $request)
    {
        try {
        
            $imagen = $request->file('image');
            $data = $request->all();
            $data['image'] = subirImagen($imagen, public_path('images/services'), $request->name);
            Service::create($data);
            return redirect('/admin/services')->with('success','Servicio agregado con éxito');
          
    }  
    catch(\Exception $e){
        Log::error(__METHOD__ . ' :: ' . $e->getMessage());
        Log::error(__METHOD__ . ' :: ' . $e->getTraceAsString());

        return redirect('/admin/services')->with('error','Error intente más tarde');   
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::find($id);  
        return view('admin.services.edit',compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceRequest $request, $id)
    {
        $service = Service::find($id);
        $imagenPrevia = $service->image;
      
        try {
            if ($request->hasFile('image')){
              
              $imagen = $request->file('image'); 
              
  
              $ruta = public_path('images/services');
              unlink($ruta.'/'.$imagenPrevia);
              unlink($ruta.'/thumbs/'.$imagenPrevia);
              $service->image = subirImagen($imagen, public_path('images/services'), $request->name);
            }
              
              $service->name= $request->name;
              $service->description= $request->description;
              $service->save();
  
              return redirect('/admin/services')->with('success','Servicio modificado con éxito');
    
       }catch(\Exception $e){
              
            Log::error(__METHOD__ . ' :: ' . $e->getMessage());
            Log::error(__METHOD__ . ' :: ' . $e->getTraceAsString());
              
            return redirect('/admin/services')->with('error','Error intente más tarde');
                
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $services = Service::find($id);
            $imagenPrevia = $services->image;
            
            $services->delete();
            $ruta = public_path('images/services');
            unlink($ruta.'/'.$imagenPrevia);
            unlink($ruta.'/thumbs/'.$imagenPrevia);
              
            return redirect('/admin/services')->with('eliminado','Servicio eliminado con éxito');
      
        } catch(\Exception $e){
                  
            Log::error(__METHOD__ . ' :: ' . $e->getMessage());
            Log::error(__METHOD__ . ' :: ' . $e->getTraceAsString());
            return redirect('/admin/services')->with('error','Error intente más tarde');        
          }
    }
}
