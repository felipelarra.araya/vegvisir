<?php
use App\Http\Controllers\IndexController;
use App\Http\Controllers\Admin\ServiceController;
use App\Http\Controllers\ContactoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/home', [IndexController::class, 'index'])->name('home');
//formulario de contacto
Route::post('/create/contact', [ContactoController::class, 'send'])->name('store.contact');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

/** ADMIN */
Route::resource('/admin/services', ServiceController::class);
